package com.flyco.tablayoutsamples.ui;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.flyco.tablayout.ViewPager2TabLayout;
import com.flyco.tablayout.widget.MsgView;
import com.flyco.tablayoutsamples.R;

import java.util.ArrayList;
import java.util.List;

public class ViewPager2Activity extends AppCompatActivity {
    private final ArrayList<Fragment> mFragments = new ArrayList<>();
    private final String[] mTitles = {"热门", "iOS", "Android", "前端", "后端", "设计", "工具资源"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager2_tab);

        for (String title : mTitles) {
            mFragments.add(SimpleCardFragment.getInstance(title));
        }

        ViewPager2 vp = findViewById(R.id.vp);
        vp.setOffscreenPageLimit(ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT);
        MyPagerAdapter mAdapter = new MyPagerAdapter(this, mFragments);
        vp.setAdapter(mAdapter);


        /** 默认 */
        ViewPager2TabLayout tabLayout_1 = findViewById(R.id.tl_1);
        /**自定义部分属性*/
        ViewPager2TabLayout tabLayout_2 = findViewById(R.id.tl_2);
        /** 字体加粗,大写 */
        ViewPager2TabLayout tabLayout_3 = findViewById(R.id.tl_3);
        /** tab固定宽度 */
        ViewPager2TabLayout tabLayout_4 = findViewById(R.id.tl_4);
        /** indicator固定宽度 */
        ViewPager2TabLayout tabLayout_5 = findViewById(R.id.tl_5);
        /** indicator圆 */
        ViewPager2TabLayout tabLayout_6 = findViewById(R.id.tl_6);
        /** indicator矩形圆角 */
        ViewPager2TabLayout tabLayout_7 = findViewById(R.id.tl_7);
        /** indicator三角形 */
        ViewPager2TabLayout tabLayout_8 = findViewById(R.id.tl_8);
        /** indicator圆角色块 */
        ViewPager2TabLayout tabLayout_9 = findViewById(R.id.tl_9);
        /** indicator圆角色块 */
        ViewPager2TabLayout tabLayout_10 = findViewById(R.id.tl_10);

        tabLayout_1.setViewPager(vp, mTitles);
        tabLayout_2.setViewPager(vp, mTitles);
        tabLayout_3.setViewPager(vp, mTitles);
        tabLayout_4.setViewPager(vp, mTitles);
        tabLayout_5.setViewPager(vp, mTitles);
        tabLayout_6.setViewPager(vp, mTitles);
        tabLayout_7.setViewPager(vp, mTitles);
        tabLayout_8.setViewPager(vp, mTitles);
        tabLayout_9.setViewPager(vp, mTitles);
        tabLayout_10.setViewPager(vp, mTitles);

        vp.setCurrentItem(4);

        tabLayout_1.showDot(4);
        tabLayout_3.showDot(4);
        tabLayout_2.showDot(4);

        tabLayout_2.showMsg(3, 5);
        tabLayout_2.setMsgMargin(3, 0, 10);
        MsgView rtv_2_3 = tabLayout_2.getMsgView(3);
        if (rtv_2_3 != null) {
            rtv_2_3.setBackgroundColor(Color.parseColor("#6D8FB0"));
        }

        tabLayout_2.showMsg(5, 5);
        tabLayout_2.setMsgMargin(5, 0, 10);
    }

    private static class MyPagerAdapter extends FragmentStateAdapter {
        private final List<Fragment> fragments;

        public MyPagerAdapter(@NonNull FragmentActivity fragmentActivity, List<Fragment> fragments) {
            super(fragmentActivity);
            this.fragments = fragments;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return fragments.get(position);
        }

        @Override
        public int getItemCount() {
            return fragments.size();
        }
    }
}
